package com.hendisantika.springbootjasperhtmlmail;

import com.hendisantika.springbootjasperhtmlmail.mail.ApplicationProperties;
import com.hendisantika.springbootjasperhtmlmail.mail.EmailService;
import com.hendisantika.springbootjasperhtmlmail.mail.ReportService;
import com.hendisantika.springbootjasperhtmlmail.storage.StorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@EnableAsync
@EnableScheduling
@SpringBootApplication
@RequiredArgsConstructor
@EnableConfigurationProperties(ApplicationProperties.class)
public class SpringBootJasperHtmlMailApplication {
    private final ReportService reportService;
    private final EmailService emailService;
    private final ApplicationProperties properties;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJasperHtmlMailApplication.class, args);
    }

    @Scheduled(cron = "${com.hendisantika.springbootjasperhtmlmail.cron}")
    void sendHTMLEmail() {
        Set<ApplicationProperties.Mail.Recipient> recipients = properties.getMail().getRecipients();

        for (ApplicationProperties.Mail.Recipient recipient : recipients) {
            Map<String, Object> params = new HashMap<>();
            params.put("username", recipient.getUsername());
            String html = reportService.generateHtmlReport("html", params);
            emailService.sendHtmlEmail(recipient.getEmail(), html);
        }
    }

    @Scheduled(cron = "${com.hendisantika.springbootjasperhtmlmail.inline-cron}")
    void sendInlineHTMLEmail() {

        Set<ApplicationProperties.Mail.Recipient> recipients = properties.getMail().getRecipients();

        for (ApplicationProperties.Mail.Recipient recipient : recipients) {
            Map<String, Object> params = new HashMap<>();
            params.put("username", recipient.getUsername());
            List<Object> result = reportService.generateInlineHtmlReport("html_inline", params);
            String html = (String) result.get(0);
            @SuppressWarnings("unchecked")
            Map<String, byte[]> imageSource = (Map<String, byte[]>) result.get(1);
            emailService.sendHtmlEmail(recipient.getEmail(), html, imageSource);
        }
    }

    @Bean
    ApplicationRunner init(StorageService storageService) {
        return args -> {
            storageService.deleteAll();
            storageService.init();
        };
    }
}
