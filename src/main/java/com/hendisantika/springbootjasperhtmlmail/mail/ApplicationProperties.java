package com.hendisantika.springbootjasperhtmlmail.mail;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper-html-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/07/18
 * Time: 08.00
 * To change this template use File | Settings | File Templates.
 */
@Data
@Validated
@ConfigurationProperties(prefix = "com.hendisantika.springbootjasperhtmlmail")
public class ApplicationProperties {

    private final @Valid Mail mail = new Mail();
    /**
     * Cron expression to schedule HTML mail
     */
    @NotBlank
    private String cron;
    /**
     * Cron expression to schedule inline HTML mail
     */
    @NotBlank
    private String inlineCron;
    /**
     * The base path where reports will be stored after compilation
     */
    @NotNull
    private Resource storageLocation;
    /**
     * The location of JasperReports source files
     */
    @NotNull
    private Resource reportLocation;

    @Data
    public static class Mail {
        @Email
        @NotNull
        private String sender;
        /**
         * The display name for the Sender
         */
        private String personal;
        private String messageSubject;
        @NotEmpty
        private Set<Recipient> recipients;

        @Data
        public static class Recipient {
            @NotBlank
            private String username;
            private String firstName;
            private String lastName;
            @Email
            @NotNull
            private String email;

        }

    }

}