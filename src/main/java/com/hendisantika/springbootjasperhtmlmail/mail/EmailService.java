package com.hendisantika.springbootjasperhtmlmail.mail;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper-html-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/07/18
 * Time: 07.58
 * To change this template use File | Settings | File Templates.
 */
public interface EmailService {
    /**
     * Sends an HTML mail
     *
     * @param recipient
     * @param html
     */
    void sendHtmlEmail(String recipient, String html);

    /**
     * Sends an HTML mail with inline images
     *
     * @param recipient
     * @param html
     * @param imageSource
     */
    void sendHtmlEmail(String recipient, String html, Map<String, byte[]> imageSource);
}
