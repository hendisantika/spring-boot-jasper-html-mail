package com.hendisantika.springbootjasperhtmlmail.mail;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper-html-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/07/18
 * Time: 07.59
 * To change this template use File | Settings | File Templates.
 */
@Async
@Slf4j
@Component
@RequiredArgsConstructor
public class HtmlEmailService implements EmailService {
    private final JavaMailSender javaMail;
    private final ApplicationProperties properties;

    /*
     * (non-Javadoc)
     *
     * @see com.juliuskrah.jasper.mail.EmailService#sendHtmlEmail(java.lang.String,
     * java.lang.String)
     */
    @Override
    public void sendHtmlEmail(String recipient, String html) {
        final MimeMessage message = javaMail.createMimeMessage();
        try {
            final MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
            helper.setFrom(properties.getMail().getSender(),
                    properties.getMail().getPersonal());
            helper.setTo(recipient);
            helper.setSubject(properties.getMail().getMessageSubject());
            log.info("Sending HTML email to {}", recipient);
            // Set to true for HTML
            helper.setText(html, true);
            javaMail.send(message);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error("Error encountered preparing MimeMessage", e);
        }

        log.debug("Sending html email completed");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.juliuskrah.jasper.mail.EmailService#sendHtmlEmail(java.lang.String,
     * java.lang.String, java.util.Map)
     */
    @Override
    public void sendHtmlEmail(String recipient, String html,
                              Map<String, byte[]> imageSource) {
        final MimeMessage message = javaMail.createMimeMessage();
        try {
            final MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setFrom(properties.getMail().getSender(),
                    properties.getMail().getPersonal());
            helper.setTo(recipient);
            helper.setSubject(properties.getMail().getMessageSubject());
            log.info("Sending HTML email to {}", recipient);
            // Set to true for HTML
            helper.setText(html, true);
            for (Map.Entry<String, byte[]> val : imageSource.entrySet()) {
                helper.addInline(val.getKey(), new ByteArrayResource(val.getValue()), "image/png");
            }
            javaMail.send(message);
        } catch (MessagingException | UnsupportedEncodingException e) {
            log.error("Error encountered preparing MimeMessage", e);
        }

        log.debug("Sending html email completed");
    }

}
