package com.hendisantika.springbootjasperhtmlmail.mail;

import com.hendisantika.springbootjasperhtmlmail.storage.StorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.MapHtmlResourceHandler;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.HtmlExporterConfiguration;
import net.sf.jasperreports.export.HtmlExporterOutput;
import net.sf.jasperreports.export.HtmlReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper-html-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/07/18
 * Time: 08.02
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class JasperReportsService implements ReportService {
    private final StorageService storageService;

    @Override
    public String generateHtmlReport(String inputFileName, Map<String, Object> params) {
        return generateHtmlReport(inputFileName, params, new JREmptyDataSource());
    }

    @Override
    public String generateHtmlReport(String inputFileName, Map<String, Object> params,
                                     JRDataSource dataSource) {
        byte[] bytes = null;
        JasperReport jasperReport = null;
        try (ByteArrayOutputStream byteArray = new ByteArrayOutputStream()) {
            // Check if a compiled report exists
            if (storageService.jasperFileExists(inputFileName)) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(storageService.loadJasperFile(inputFileName));
            }
            // Compile report from source and save
            else {
                String jrxml = storageService.loadJrxmlFile(inputFileName);
                log.info("{} loaded. Compiling report", jrxml);
                jasperReport = JasperCompileManager.compileReport(jrxml);
                // Save compiled report. Compiled report is loaded next time
                JRSaver.saveObject(jasperReport,
                        storageService.loadJasperFile(inputFileName));
            }
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params,
                    dataSource);
            Exporter<ExporterInput, HtmlReportConfiguration, HtmlExporterConfiguration, HtmlExporterOutput> exporter;
            exporter = new HtmlExporter();

            exporter.setExporterOutput(new SimpleHtmlExporterOutput(byteArray));
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.exportReport();
            bytes = byteArray.toByteArray();
        } catch (JRException | IOException e) {
            e.printStackTrace();
            log.error("Encountered error when loading jasper file", e);
        }

        return new String(bytes);
    }

    @Override
    public List<Object> generateInlineHtmlReport(String inputFileName,
                                                 Map<String, Object> params) {
        return generateInlineHtmlReport(inputFileName, params, new JREmptyDataSource());
    }

    @Override
    public List<Object> generateInlineHtmlReport(String inputFileName,
                                                 Map<String, Object> params, JRDataSource jRDataSource) {
        JasperReport jasperReport = null;
        List<Object> result = new ArrayList<>();

        // Map<byte[], Map<String, byte[]>> result = new HashMap<>();
        try (ByteArrayOutputStream byteArray = new ByteArrayOutputStream()) {
            if (storageService.jasperFileExists(inputFileName)) {
                jasperReport = (JasperReport) JRLoader
                        .loadObject(storageService.loadJasperFile(inputFileName));
            } else {
                String jrxml = storageService.loadJrxmlFile(inputFileName);
                log.debug("{} loaded. Compiling report", jrxml);
                jasperReport = JasperCompileManager.compileReport(jrxml);
                JRSaver.saveObject(jasperReport,
                        storageService.loadJasperFile(inputFileName));
            }
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params,
                    jRDataSource);
            Exporter<ExporterInput, HtmlReportConfiguration, HtmlExporterConfiguration, HtmlExporterOutput> exporter;
            exporter = new HtmlExporter();
            Map<String, byte[]> resourcesMap = new HashMap<>();
            SimpleHtmlExporterOutput htmlExporterOutput = new SimpleHtmlExporterOutput(
                    byteArray);
            htmlExporterOutput
                    .setImageHandler(new MapHtmlResourceHandler((resourcesMap)) {
                        @Override
                        public String getResourcePath(String id) {
                            return "cid:" + id;
                        }

                    });

            exporter.setExporterOutput(htmlExporterOutput);
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.exportReport();
            String html = new String(byteArray.toByteArray());
            result.add(html);
            result.add(resourcesMap);
        } catch (JRException | IOException e) {
            log.error("Encountered error when loading jasper file", e);
        }

        return result;
    }

}