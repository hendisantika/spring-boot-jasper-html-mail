package com.hendisantika.springbootjasperhtmlmail.storage;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper-html-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/07/18
 * Time: 08.05
 * To change this template use File | Settings | File Templates.
 */
public class StorageException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}