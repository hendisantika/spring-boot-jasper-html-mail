package com.hendisantika.springbootjasperhtmlmail.storage;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper-html-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/07/18
 * Time: 08.06
 * To change this template use File | Settings | File Templates.
 */
public class StorageFileNotFoundException extends StorageException {
    private static final long serialVersionUID = 1L;

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}