package com.hendisantika.springbootjasperhtmlmail.storage;

import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper-html-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/07/18
 * Time: 08.05
 * To change this template use File | Settings | File Templates.
 */
public interface StorageService {
    void init();

    void deleteAll();

    boolean jrxmlFileExists(String file);

    boolean jasperFileExists(String file);

    String loadJrxmlFile(String file);

    File loadJasperFile(String file);
}
